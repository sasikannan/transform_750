{{ config(materialized='table', unique_key='account_id') }}

with account_enhanced as (

  SELECT account_id, pending_invoices
  from {{ env_var('db_name') }}.{{ env_var('db_schema') }}.account_enhanced

),

final as (

    select account_id,pending_invoices
    from account_enhanced

)

select * from final
