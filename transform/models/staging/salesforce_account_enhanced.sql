{{ config(materialized='incremental',unique_key='account_id',incremental_strategy='insert_overwrite') }}
with sf_account as (
    select
         account_id as account_id,

         pending_invoices as pending_invoices

    from {{ env_var('db_name') }}.{{ env_var('db_schema') }}.salesforce_account_enhanced

),

account_enhanced as (

    select
        account_id,
        pending_invoices,
        CURRENT_TIMESTAMP as event_time,
        CURRENT_TIMESTAMP as update_time

    from sf_account

)

select * from account_enhanced
